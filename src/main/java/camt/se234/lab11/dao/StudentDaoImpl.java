package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("1","1","temp",1.33));
        students.add(new Student("2","2","temp",2.33));
        students.add(new Student("3","3","temp",3.33));
        students.add(new Student("4","4","temp",4.00));
        students.add(new Student("5","5","temp",4.00));

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentDaoImpl)) return false;
        StudentDaoImpl that = (StudentDaoImpl) o;
        return Objects.equals(students, that.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(students);
    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
