package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("1"),is(new Student("1","1","temp",1.33)));
        assertThat(studentService.findStudentById("2"),is(new Student("2","2","temp",2.33)));
        assertThat(studentService.findStudentById("3"),is(new Student("3","3","temp",3.33)));
        assertThat(studentService.findStudentById("4"),is(new Student("4","4","temp",4.00)));
    }
    @Test
    public void testGetAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.998));
    }
    @Test
    public void testWithMock(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }
    @Test
    public void testFindByPartOfId(){

        List<Student> mockStudents= new ArrayList<>();

        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22")
                ,hasItems(new Student("223","C","temp",2.33)
                        ,new Student("224","D","temp",2.33)));
    }
    @Test
    public void testFindByPartOfId2(){

        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("331","1","temp",3.33));
        mockStudents.add(new Student("332","2","temp",3.33));
        mockStudents.add(new Student("333","3","temp",3.33));
        mockStudents.add(new Student("334","4","temp",3.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("33"),hasItem(new Student("333","3","temp",3.33)));
        assertThat(studentService.findStudentByPartOfId("33")
                ,hasItems(new Student("333","3","temp",3.33)
                        ,new Student("334","4","temp",3.33)));
    }
    @Test
    public void testWithMockAvgGpa(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("331","1","temp",1.33));
        mockStudents.add(new Student("332","2","temp",1.33));
        mockStudents.add(new Student("333","3","temp",1.33));
        mockStudents.add(new Student("334","4","temp",1.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(1.33));
    }
    @Test(expected = NoDataException.class)
    public void testNoDataException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("1","1","temp",1.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("2"),nullValue());

    }
    @Test(expected = ArraySizeIsZero.class)
    public void testArrayIsZeroException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("1","1","temp",1.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("2"),nullValue());

    }
    @Test(expected = Arithmetic.class)
    public void testDataIsDividedByZeroException(){
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());

    }
}
